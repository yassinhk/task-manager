import React from 'react'
import Input from '../../Components/Input/Input';
const Login = () => {
    return (
        <div className='bg-gray-300 h-screen flex justify-center pt-20 font-inter'>
            <div className='w-full max-w-xs'>
                <form className='bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4'>
                    <h1 className='font-koulen text-3xl py-8 text-center text-blue-900'>
                        Task Manager
                    </h1>
                    <Input 
                        id="username"
                        label="Username"
                        type="text"
                        placeholder="Username"
                    />
                    <Input 
                        id="password"
                        label="Password"
                        type="password"
                        placeholder="************"
                    />
                    <button 
                        className='shadow cursor-pointer appearance-none border rounded w-full mt-4 mb-8 py-3 px-3 bg-blue-900 text-white leading-tight focus:outline-none focus:shadow-outline focus:bg-blue-700'
                    >
                        Connexion    
                    </button>
                </form>
                
            </div>
        </div>
    );
};

export default Login;