import { Navigate, Outlet, Routes, Route } from 'react-router-dom';
import Page404 from '../404/Page404';
import Home from '../Home/Home';
import Login from '../Login/Login'

const userAuth = ()=>{
    const user = {isLoggedIn: false}
    return user && user.isLoggedIn
}

const ProtectedRoutes = ()=>{
    const isAuth = userAuth()
    return isAuth ? 
        <Outlet /> : 
        <Navigate 
            to="/auth"   
        />
}


const App = () =>{
    return(
        <Routes>
                <Route path="/auth" element={<Login/>}/>
                <Route  element={<ProtectedRoutes />}>
                <Route path="/" element={<Home/>}/>
            </Route>
            <Route path="*" element={<Page404/>}/>
        </Routes>
    )
}

export default App;
