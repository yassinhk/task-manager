import React from 'react';
import { Link } from 'react-router-dom';

const ArrowLeft = ()=>{
    return(
        <svg xmlns="http://www.w3.org/2000/svg"
            className="pl-2 h-6 w-6" 
            fill="none" 
            viewBox="0 0 24 24" 
            stroke="currentColor" 
            strokeWidth={2}>
            <path strokeLinecap="round" strokeLinejoin="round" d="M17 8l4 4m0 0l-4 4m4-4H3" />
        </svg>
    )
}

const Page404 = () => {
    return (
        <div className='flexbox py-40 text-center h-screen bg-gray-100 font-sans'>
            <h4 className='text-blue-600 text-lg uppercase font-bold'>404 Error</h4>
            <h1 className='font-bold text-5xl pb-4 '>Page not found</h1>
            <h3 className='font-medium text-gray-500 pb-5'>Sorry, we couldn't find the page you're looking for.</h3>
            <Link to="/" className='inline-flex bg-blue-600 p-4 rounded-md text-white'>
                Go back home
                <ArrowLeft/>        
            </Link>
        </div>
    );
};

export default Page404;