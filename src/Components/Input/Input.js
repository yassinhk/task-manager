import React from 'react';

const Input = props => {
    return (
        <div className="mb-4">
            <label className="block text-gray-700 text-sm font-bold mb-2" for={props.id}>
                {props.label}
            </label>
            <input 
                className="shadow appearance-none border focus:border-blue-900 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" 
                id={props.id} 
                type={props.type}
                placeholder={props.placeholder}
            />
            <p className="hidden text-red-500 mt-2 text-xs italic">Please choose a {props.id}.</p> 
        </div>
    );
};

export default Input;