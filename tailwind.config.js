module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
    fontFamily : {
        sans : ['Robotto', 'sans-serif'],
        radioCanada : ['Radio Canada', 'sans-serif'] ,
        koulen : ['koulen', 'sans-serif'] ,
        inter : ['inter', 'sans-serif'] ,
    }
  },
  plugins: [],
}
